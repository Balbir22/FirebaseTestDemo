package net.simplifiedcoding.fcm;


public class Constants {

    //Firebase app url
    public static final String FIREBASE_APP = "https://testfirebasedemo-cd5e1.firebaseio.com";

    //Constant to store shared preferences
    public static final String SHARED_PREF = "mynotificationapp";

    //To store boolean in shared preferences for if the device is registered to not
    public static final String REGISTERED = "registered";

    //To store the firebase id in shared preferences
    public static final String UNIQUE_ID = "uniqueid";

    //register.php address in your server
    public static final String REGISTER_URL = "http://192.168.94.1/firebasepushnotification/register.php";

}
