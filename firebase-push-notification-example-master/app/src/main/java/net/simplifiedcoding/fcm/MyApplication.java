package net.simplifiedcoding.fcm;

import android.app.Application;

import com.firebase.client.Firebase;


public class MyApplication extends Application{

    @Override
    public void onCreate() {
        super.onCreate();
        //Initializing firebase
        Firebase.setAndroidContext(getApplicationContext());
    }
}
